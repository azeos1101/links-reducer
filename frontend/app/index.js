const is_valid = (str) => {
  return str.length != 0 && /^(https?:\/\/)?[a-zA-Z\d-]+(\.[a-zA-Z\d-]+)+(\:\d+)?(\/{1}.*)?$/.test(str);
}

const get_reduced_url = (original_url) => {
  $.post( '/', { data: original_url } )
    .done( ( data ) => {
      $('#url').val('');
      $("#short_url a").attr("href", data.trim()).text(data.trim().replace(/^https?:\/\//, ''));
    }
  );
}

$("form").on("submit",  (event) => {
    event.preventDefault();
    urlData = $('#url').val().trim();
    if (!is_valid(urlData)) {
      return;
    }
    get_reduced_url(urlData);
  }
);
