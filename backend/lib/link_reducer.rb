require 'redis'
require 'digest'
require 'uri'
require 'rack'

module LinkReducer
  ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'\
          'abcdefghijklmnopqrstuvwxyz'\
          '0123456789-_'.freeze

  class Handler
    def initialize
      @code_length = ENV['CODE_LENGTH'].to_i || 5
      @server_url = ENV['HOST']
      @server_port = ENV['PORT'] ? ":#{ENV['PORT']}" : ''
      @base = (LinkReducer::ALPHA.length - 1)
      @db = Redis.new(host: 'redis', port: 6379, db: 0)
    end

    def call(env)
      req = Rack::Request.new(env)
      res = {}
      case req.request_method
      when 'POST'
        res = post(req)
      when 'GET'
        res = get(req)
      end
      [res['code'] || '200', res['headers'] || {}, res['body'] || []]
    end

    private

    def get_substr_hash(str, count, step)
      result = {}
      count.times do |i|
        result["s#{i}"] = str[i * step, step]
      end
      result
    end

    def gen_key(url_str)
      crypted_str = Digest::SHA1.hexdigest(url_str)
      parts_count = crypted_str.size / @code_length
      substr_hash = get_substr_hash(crypted_str, parts_count, @code_length)
      key = ''
      @code_length.times do |i|
        sum = 0
        substr_hash.keys.each { |k| sum += (substr_hash[k][i]).hex }
        key += LinkReducer::ALPHA[sum % @base]
      end
      key
    end

    def get_code(url_str)
      code = gen_key(url_str)
      code = get_code(url_str + ' ') if (url = @db.get(code)) && url != url_str
      code
    end

    def post(req)
      res = {}
      url_str = req.body.read.gsub(/^data=/, '')
      url_str = URI.decode_www_form_component url_str
      url_str = 'http://' + url_str unless url_str =~ %r{^https?:\/\/.*}
      code = get_code(url_str)
      status = @db.set(code, url_str)
      res['headers'] = { 'Content-Type' => 'text/plain', 'X-Status' => status }
      res['body'] = ["#{@server_url}#{@server_port}/#{code}", "\r\n"]
      res
    end

    def get(req)
      res = {}
      code = req.path.gsub(%r{^\/}, '')
      url = @db.get(code)
      res['code'] = '301'
      res['headers'] = { 'Location' => url.to_s }
      res
    end
  end
end
