require 'rack'
require './lib/link_reducer'

use Rack::Reloader if ENV['ENV'] == 'development'

run LinkReducer::Handler.new
