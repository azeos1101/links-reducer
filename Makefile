build-frontend:
	docker build -f ./frontend/Dockerfile ./frontend/ -t lr-frontend

build-backend:
	docker build -f ./backend/Dockerfile ./backend/ -t lr-backend

build: build-frontend build-backend

up:
	docker-compose up

down:
	docker-compose down

start: up

stop:
	docker-compose stop

lint:
	rubocop

test: lint
